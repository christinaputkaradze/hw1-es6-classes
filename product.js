class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set name(value) {
        this._name = value;
    }
    get name() {
        return this._name;
    }

    set age(value) {
        this._age = value;
    }
    get age() {
        return this._age;
    }

    set salary(value) {
        this._salary = value;
    }
    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    set salary(value) {
        this._salary = value;
    }
    // чи варто так записувати?
    // set salary(value) {
    //     super.salary = value;
    // }

    get salary() {
        return this._salary * 3;
    }
}

let programmer1 = new Programmer('Anya', 23, 3000, 'react');
console.log(programmer1);
console.log(programmer1.salary);
programmer1.name = 'Vasil';
programmer1.age = 58;
programmer1.salary = 15000;
programmer1.lang = 'python';
console.log(programmer1);
console.log(programmer1.salary);



let programmer2 = new Programmer('Bogdan', 26, 6000, 'c sharp');
console.log(programmer2);
console.log(programmer2.salary);

let programmer3 = new Programmer('Maxim', 30, 10000, 'python');
console.log(programmer3);
console.log(programmer3.salary);

